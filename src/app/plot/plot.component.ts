import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, OnInit }            from '@angular/core';
import { DomSanitizer }                 from '@angular/platform-browser';

@Component({
    selector   : 'app-plot',
    templateUrl: './plot.component.html',
    styleUrls  : ['./plot.component.scss']
})
export class PlotComponent implements OnInit {
    plot: any;
    url = 'http://localhost:63342/fredtrade_ui/src/assets/plots/XRP_BTC.html';
    url2: any;

    constructor(private http: HttpClient,
                private sanitizer: DomSanitizer) {
    }

    ngOnInit() {
        this.url2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
    }

}
