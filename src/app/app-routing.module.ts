import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayComponent }     from './display/display.component';
import { PlotComponent }        from './plot/plot.component';
import { SigninComponent }      from './signin/signin.component';

const routes: Routes = [
    {
        path: 'signin',
        component: SigninComponent
    },
    {
        path: 'plot',
        component: PlotComponent
    },
    {
        path: '',
        component: DisplayComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
