import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XRPBTCComponent } from './xrp-btc.component';

describe('XRPBTCComponent', () => {
  let component: XRPBTCComponent;
  let fixture: ComponentFixture<XRPBTCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XRPBTCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XRPBTCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
