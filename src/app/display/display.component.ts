import { Component, OnInit }                  from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService }                         from '../api.service';

@Component({
    selector   : 'app-display',
    templateUrl: './display.component.html',
    styleUrls  : ['./display.component.scss']
})
export class DisplayComponent implements OnInit {
    data: any;
    form: FormGroup;

    constructor(private apiService: ApiService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.getData(true);

        this.form = this.formBuilder.group({
            exchange : ['binance', Validators.required],
            period   : [10, Validators.required],
            pair     : ['ast/btc', Validators.required],
            strategy1: ['strategy001', Validators.required],
            strategy2: ['DefaultStrategy', Validators.required]
        });
    }

    submit() {
        this.getData(false);
    }

    getData(defaultSearch = true) {
        let parameters = {};
        if (defaultSearch) {
            parameters['exchange'] = 'binance';
            parameters['period'] = 10;
            parameters['pair'] = 'ast/btc';
            parameters['strategy1'] = 'strategy001';
            parameters['strategy2'] = 'DefaultStrategy';
        } else {
            parameters['exchange'] = this.form.controls.exchange.value;
            parameters['period'] = this.form.controls.period.value;
            parameters['pair'] = this.form.controls.pair.value;
            parameters['strategy1'] = this.form.controls.strategy1.value;
            parameters['strategy2'] = this.form.controls.strategy2.value;
        }
        this.apiService.getData(parameters).subscribe(data => {
            console.log(data);
            this.data = data.data;
            this.data.map(row => {
                row.open_time = new Date(row.open_time).toLocaleString();
                row.close_time = new Date(row.close_time).toLocaleString();
            });
        });
    }
}
