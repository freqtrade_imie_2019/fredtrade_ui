import { HttpClientModule }          from '@angular/common/http';
import { NgModule }                  from '@angular/core';
import { ReactiveFormsModule }       from '@angular/forms';
import { BrowserModule }             from '@angular/platform-browser';
import { NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ApiService }                from './api.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent }     from './app.component';
import { DisplayComponent } from './display/display.component';
import { SigninComponent }  from './signin/signin.component';
import { PlotComponent } from './plot/plot.component';
import { XRPBTCComponent } from './xrp-btc/xrp-btc.component';

@NgModule({
    declarations: [
        AppComponent,
        SigninComponent,
        DisplayComponent,
        PlotComponent,
        XRPBTCComponent
    ],
    imports     : [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        NgbAlertModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers   : [
        ApiService
    ],
    bootstrap   : [AppComponent]
})
export class AppModule {
}
